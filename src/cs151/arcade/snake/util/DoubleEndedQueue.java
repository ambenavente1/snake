package cs151.arcade.snake.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * DoubleEndedQueue data structure for use in the arcade program
 * (specifically the snake program)
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/5/14
 */
public class DoubleEndedQueue<T> implements Iterable {

    /**
     * The head of this list.  This is a dummy-node (contains no data).
     */
    private Node<T> head;

    /**
     * The tail of this list.  This is a dummy-node (contains no data).
     */
    private Node<T> tail;

    /**
     * The size of this list
     */
    private int size;

    /**
     * Create a new DoubleEndedQueue object
     */
    public DoubleEndedQueue() {
        head = new Node<T>(null, null, null);
        tail = new Node<T>(null, null, null);
        head.next = tail;
        tail.prev = head;
    }

    /**
     * Adds a node to the top of the list
     *
     * @param data The data to add to the top of the list
     */
    public void addFirst(T data) {
        Node<T> node = new Node<T>(data, head, head.next);

        if (size < 1) {
            tail.prev = node;
        } else {
            head.next.prev = node;
        }

        head.next = node;

        size++;
    }

    /**
     * Adds a node to the bottom of the list
     *
     * @param data The data to add to the bottom of the list
     */
    public void addLast(T data) {
        Node<T> node = new Node<T>(data, tail.prev, tail);

        if (size < 1) {
            head.next = node;
        } else {
            tail.prev.next = node;
        }

        tail.prev = node;

        size++;
    }

    /**
     * Gets the data at the node of the specified index
     *
     * @param index The index of the node to get data from
     * @return The data of the node at the specified index
     */
    public T get(int index) {
        Node<T> start = head.next;
        if (index >= 0 && index < size) {
            while (index-- > 0) {
                start = start.next;
            }
        } else {
            throw new IndexOutOfBoundsException("The index does not exist " +
                    "in the list: (" + index + ")");
        }
        return start.data;
    }

    /**
     * Removes the top node in the list
     * @throws java.util.NoSuchElementException if there are no elements in
     * the list
     */
    public void removeFirst() {
        if (size == 0) {
            throw new NoSuchElementException("This list is empty.");
        }

        if (size > 1) {
            head.next = head.next.next;
            head.next.prev = head;
        } else {
            head.next = tail;
            tail.prev = head;
        }
        size--;
    }

    /**
     * Removes the last node in the list
     * @throws java.util.NoSuchElementException if there are no elements in
     * the list
     */
    public void removeLast() {
        if (size == 0) {
            throw new NoSuchElementException("This list is empty.");
        }

        if (size > 1) {
            tail.prev = tail.prev.prev;
            tail.prev.next = tail;
        } else {
            tail.prev = head;
            head.next = tail;
        }
        size--;
    }

    /**
     * Gets the data in the first node of this list
     *
     * @return the data in the first node of this list
     * @throws NullPointerException if there are no nodes in the
     * list
     */
    public T examineFirst() {
        return (T)head.next.data;
    }

    /**
     * Gets the data in the last node of this list
     *
     * @return the data in the last node in this list
     * @throws NullPointerException if there are no nodes in the
     * list
     */
    public T examineLast() {
        return (T)tail.prev.data;
    }

    /**
     * Gets how many nodes are in this list (not including the 2 dummy nodes)
     *
     * @return The size of the list (how many usable nodes are in the list)
     */
    public int size() {
        return size;
    }

    /**
     * Gets if the list is empty (there are no usable nodes in this list).
     * This does not include the two dummy nodes (head/tail)
     *
     * @return If the list is empty (if there are no usable nodes in the
     * list).
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Gets the iterator used to traverse this queue
     *
     * @return The iterator for traversing the queue
     */
    @Override
    public Iterator iterator() {
        return new DoubleEndedQueueIterator();
    }

    /**
     * The iterator for the DoubleEndedQueue
     */
    private class DoubleEndedQueueIterator implements Iterator {

        /**
         * The node to start at (will be the head of the queue)
         */
        private Node<T> start;

        /**
         * Creates a new iterator with the head of the queue as the starting
         * point of traversal
         */
        private DoubleEndedQueueIterator() {
            start = head;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return start.next != tail;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws java.util.NoSuchElementException if the iteration has
         * no more elements
         */
        @Override
        public Object next() {
            return (start = start.next).data;
        }

        /**
         * Remove is not supported for this iterator
         */
        @Override
        public void remove() {
            throw new RemoveNotSupportedException();
        }
    }
}
