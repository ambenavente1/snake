package cs151.arcade.snake.util;

/**
 * Individual node in a linked list that contains a specified type of data
 * (generic).  I purposely left off <em>public</em> and <em>private</em> to
 * make the fields and constructor in this class <em>package</em> because
 * I only intend on writing ADTs in this package.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/5/14
 */
public class Node<T> {

    /**
     * The data that this node has
     */
    T data;

    /**
     * The node that comes before this one in a linked data structure
     */
    Node prev;

    /**
     * The node that comes after this one in a linked data structure
     */
    Node next;

    /**
     * Creates a new Node object
     *
     * @param data The data for this node to hold
     * @param prev The node that comes before this one
     * @param next The node that comes after this one
     */
    Node(T data, Node prev, Node next) {
        this.data = data;
        this.prev = prev;
        this.next = next;
    }

    /**
     * Prints out this node's reference, the previous node's reference,
     * the next node's reference, and this node's data
     *
     * @return A String containing the information in the description
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{\n");
        sb.append("    [this: " + this.reference() + "]\n");
        sb.append("    [prev: " + prev.reference() + "]\n");
        sb.append("    [next: " + next.reference() + "]\n");
        sb.append("    [data: " + data + "]\n");
        sb.append("}");

        return sb.toString();
    }

    /**
     * Gets the reference address for this node
     *
     * @return The reference address for this node (everything in default
     * toString not including everything before the @)
     */
    private String reference() {
        // Only prints out the reference address
        return super.toString().split("@")[1];
    }
}
