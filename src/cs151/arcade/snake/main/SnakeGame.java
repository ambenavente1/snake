package cs151.arcade.snake.main;

import cs151.arcade.snake.states.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public class SnakeGame extends StateBasedGame {

    public static final String TITLE = "Snake";
    public static final int GAME_WIDTH = 640;
    public static final int GAME_HEIGHT = 480;

    public static final Color DEFAULT_COLOR = Color.black;

    /**
     * Create a new state based game with the default title, "Snake"
     */
    public SnakeGame() {
        super(TITLE);
    }

    /**
     * Initialise the list of states making up this game
     *
     * @param container The container holding the game
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise the state based game resources
     */
    @Override
    public void initStatesList(GameContainer container)
            throws SlickException {
        addState(new MainMenuState(this));
        addState(new GameplayClassicState());
        addState(new GameplayNewState());
        addState(new QuitState());
        enterState(States.MAIN_MENU.getID(), null, new FadeInTransition());
    }

}
