package cs151.arcade.snake;

import cs151.arcade.snake.main.SnakeGame;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

/**
 * Entry point to the snake game.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public class Start {
    public static void main(String[] args) {
        try {
            AppGameContainer container
                    = new AppGameContainer(new SnakeGame());
            container.setDisplayMode(SnakeGame.GAME_WIDTH,
                                     SnakeGame.GAME_HEIGHT,
                                     false);
            container.setTargetFrameRate(60);
            container.setShowFPS(false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
