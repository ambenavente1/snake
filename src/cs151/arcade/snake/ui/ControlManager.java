package cs151.arcade.snake.ui;

import cs151.arcade.snake.objects.interfaces.Renderable;
import cs151.arcade.snake.objects.interfaces.Updatable;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/14
 */
public class ControlManager implements Renderable, Updatable {
    private List<Control> controls;

    public ControlManager() {
        controls = new ArrayList<Control>();
    }

    public ControlManager(int maxSize) {
        controls = new ArrayList<Control>(maxSize);
    }

    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        for (int i = controls.size() - 1; i >= 0; i--) {
            controls.get(i).render(container, game, g);
        }
    }

    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        for (int i = controls.size() - 1; i >= 0; i--) {
            controls.get(i).update(container, game, delta);
        }
    }

    public void add(Control c) {
        controls.add(c);
    }

    public void remove(Control c) {
        if (controls.contains(c)) {
            remove(controls.indexOf(c));
        }
    }

    public void remove(int index) {
        if (index >= 0 && index < controls.size()) {
            controls.remove(index);
        }
    }
}
