package cs151.arcade.snake.objects;

import cs151.arcade.snake.main.SnakeGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/14
 */
public class SnakeClassic extends Snake {

    @Override
    public boolean isDead() {
        for (Object o : parts) {
            SnakeSquare s = (SnakeSquare) o;
            if (s != parts.examineFirst()) {
                if (getHeadPos().equals(s.getPos())) {
                    return true;
                }
            }
        }

        return getHeadPos().x < 0 ||
               getHeadPos().x > SnakeGame.GAME_WIDTH - 1 ||
               getHeadPos().y < 0 ||
               getHeadPos().y > SnakeGame.GAME_HEIGHT - 1;
    }

    protected enum Direction {
        NORTH, EAST, SOUTH, WEST
    }

    private Direction direction;

    public SnakeClassic() {
        super(SnakeSquare.class);

        this.direction = Direction.EAST;
    }

    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        direction = checkUserInput(container.getInput());

        if (addElapsedTime(delta) > MOVE_TIME) {
            setTimeSinceMove(0);
            Vector2f newPos = getHeadPos();

            if (direction == Direction.NORTH) {
                newPos.y -= SnakePart.DEFAULT_SIZE;
            } else if (direction == Direction.EAST) {
                newPos.x += SnakePart.DEFAULT_SIZE;
            } else if (direction == Direction.SOUTH) {
                newPos.y += SnakePart.DEFAULT_SIZE;
            } else {
                newPos.x -= SnakePart.DEFAULT_SIZE;
            }

            move(newPos);
        }
    }

    /**
     * Gets the direction from the user inputting by pressing the arrow
     * keys.
     * <p></p>
     * <table>
     *     <tr>
     *         <td>Arrow Left</td>
     *         <td>West</td>
     *     </tr>
     *     <tr>
     *         <td>Arrow Right</td>
     *         <td>East</td>
     *     </tr>
     *     <tr>
     *         <td>Arrow Up</td>
     *         <td>North</td>
     *     </tr>
     *     <tr>
     *         <td>Arrow Down</td>
     *         <td>South</td>
     *     </tr>
     * </table>
     * @param input The input manager retrieved from the GameContainer's
     *              input
     * @return the direction the snake should go
     * @see org.newdawn.slick.GameContainer#getInput()
     */
    private Direction checkUserInput(Input input) {
        Direction dir = direction;

        // Only left OR right may be pressed
        if (input.isKeyDown(Input.KEY_LEFT)
                && !input.isKeyDown(Input.KEY_UP)
                && !input.isKeyDown(Input.KEY_DOWN)
                && direction != Direction.EAST) {
            dir = Direction.WEST;
        } else if (input.isKeyDown(Input.KEY_RIGHT)
                && !input.isKeyDown(Input.KEY_UP)
                && !input.isKeyDown(Input.KEY_DOWN)
                && direction != Direction.WEST) {
            dir = Direction.EAST;
        } else if (input.isKeyDown(Input.KEY_UP)
                && direction != Direction.SOUTH) {
            dir = Direction.NORTH;
        } else if (input.isKeyDown(Input.KEY_DOWN)
                && direction != Direction.NORTH) {
            dir = Direction.SOUTH;
        }

        return dir;
    }

    protected void setDirection(Direction dir) {
        direction = dir;
    }
}
