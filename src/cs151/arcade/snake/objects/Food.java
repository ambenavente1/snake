package cs151.arcade.snake.objects;

import cs151.arcade.snake.main.SnakeGame;
import org.newdawn.slick.Color;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public class Food extends StaticObject {

    private static Random rand = new Random();

    private static int SIZE = SnakeSquare.DEFAULT_SIZE;

    public Food() {
        setRandPos();
        initImage();
        setWidth(SIZE);
        setHeight(SIZE);
    }

    /**
     * Call this to initialize the image of this GameObject
     */
    @Override
    protected void initImage() {
        setColor(Color.red);
    }

    public void setRandPos() {
        setX(rand.nextInt(SnakeGame.GAME_WIDTH  / SIZE) * SIZE);
        setY(rand.nextInt(SnakeGame.GAME_HEIGHT / SIZE) * SIZE);
    }
}
