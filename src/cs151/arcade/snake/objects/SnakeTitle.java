package cs151.arcade.snake.objects;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.Point;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/2014
 */
public class SnakeTitle extends SnakeClassic {

    private Point[] sPoints = new Point[] {
            new Point(0, 4),
            new Point(0, 5),
            new Point(1, 5),
            new Point(2, 5),
            new Point(3, 5),
            new Point(3, 4),
            new Point(3, 3),
            new Point(3, 2),
            new Point(2, 2),
            new Point(1, 2),
            new Point(0, 2),
            new Point(0, 1),
            new Point(0, 0),
            new Point(1, 0),
            new Point(2, 0),
            new Point(3, 0)
    };

    private boolean started;

    public SnakeTitle(int minX, int minY, int maxX, int maxY) {

        int originalSize = parts.size();
        for (int i = 0; i < originalSize; i++) {
            parts.removeFirst();
        }

        final int xOffset = 14;
        final int yOffset = 3;

        for (Point p : sPoints) {
            parts.addFirst(new SnakeSquare(getScaledPos(new Vector2f(p.x +
                    xOffset, p.y + yOffset))));
        }

        started = false;
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        if (container.getInput().isKeyPressed(Input.KEY_RIGHT)) {
            started = true;
        }

        if (started) {
            super.update(container, game, delta);
        }
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}
