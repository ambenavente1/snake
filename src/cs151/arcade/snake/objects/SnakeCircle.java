package cs151.arcade.snake.objects;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public class SnakeCircle extends SnakePart {

    /**
     * The width of the stroke for the circle's border
     */
    private static final float BORDER_WIDTH = 2F;

    /**
     * Color of the border of the circle
     */
    private static final Color STROKE_COLOR =  new Color(0x000000);

    public SnakeCircle(Vector2f pos) {
        super(pos);
    }

    /**
     * This is just a circular image
     */
    @Override
    protected void initImage() {
        // No image is needed for this snake part
    }

    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        g.setAntiAlias(true);
        g.setColor(Color.white);
        g.fillOval(getX(), getY(), getWidth(), getHeight());
        Color originalColor = g.getColor();
        g.setColor(STROKE_COLOR);
        float line = g.getLineWidth();
        g.setLineWidth(BORDER_WIDTH);
        g.drawOval(getX(), getY(), getWidth(), getHeight());
        g.setColor(originalColor);
        g.setLineWidth(line);
        g.setAntiAlias(false);
    }
}
