package cs151.arcade.snake.objects;

import cs151.arcade.snake.objects.interfaces.Renderable;
import cs151.arcade.snake.objects.interfaces.Updatable;
import cs151.arcade.snake.util.DoubleEndedQueue;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public abstract class Snake implements Renderable, Updatable {

    protected static final int MOVE_TIME = 60;

    /**
     * Starting number of parts the snake has
     */
    private static final int START_SIZE = 5;

    /**
     * Dequeue containing snake parts
     */
    protected DoubleEndedQueue<SnakePart> parts;

    /**
     * Time since the snake last moved
     */
    protected int timeSinceMove;

    public Snake(Class c) {
        this.timeSinceMove = 0;
        this.parts = new DoubleEndedQueue<SnakePart>();

        for (int i = 0; i < START_SIZE; i++) {
            if (c == SnakeCircle.class) {
                parts.addFirst(new SnakeCircle(getScaledPos(new Vector2f(i,
                       0))));
            } else if (c == SnakeSquare.class) {
                parts.addFirst(new SnakeSquare(getScaledPos(new Vector2f(i,
                        0))));
            } else {
                throw new IllegalArgumentException("Not a snake part");
            }
        }
    }

    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        for (Object o : parts) {
            ((SnakePart)o).render(container, game, g);
        }
    }


    /**
     * Gets the position of the first node in the queue (the head of the
     * snake)
     *
     * @return the point where the snake's head is located
     */
    public Vector2f getHeadPos() {
        return parts.examineFirst().getPos().copy();
    }

    /**
     * Gets the bounding rectangle for the head node
     *
     * @return the rectangle representing the collision area for the head
     */
    public Rectangle getHeadBounds() {
        return parts.examineFirst().getBounds();
    }

    /**
     * Gets the appropriate position of a vector that is multiplied by the
     * size of the snake's square
     *
     * @param pos the position to scale
     * @return the new position scaled by a factor of the default size of a
     * SnakeSquare
     * @see cs151.arcade.snake.objects.SnakeSquare#DEFAULT_SIZE
     */
    public Vector2f getScaledPos(Vector2f pos) {
        return new Vector2f(pos.x * SnakePart.DEFAULT_SIZE,
                pos.y * SnakePart.DEFAULT_SIZE);
    }

    public void setTimeSinceMove(int timeSinceMove) {
        this.timeSinceMove = timeSinceMove;
    }

    protected void move(Vector2f newPos) {
        SnakePart newHead = parts.examineLast();
        parts.removeLast();
        newHead.setPos(newPos);
        parts.addFirst(newHead);
    }

    protected float addElapsedTime(int delta) {
        return timeSinceMove += delta;
    }

    public void add() {
        parts.addLast(parts.examineFirst() instanceof SnakeSquare
                    ? new SnakeSquare(parts.examineLast().getPos())
                    : new SnakeCircle(parts.examineLast().getPos()));
    }

    public abstract boolean isDead();
}
