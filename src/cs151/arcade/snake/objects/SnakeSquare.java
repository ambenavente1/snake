package cs151.arcade.snake.objects;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public class SnakeSquare extends SnakePart {

    public SnakeSquare(Vector2f pos) {
        super(pos);
        initImage();
    }

    /**
     * Doesn't use an image, just a square
     */
    @Override
    protected void initImage() {
        setColor(Color.white);
    }
}
