package cs151.arcade.snake.objects;

import cs151.arcade.snake.objects.interfaces.Renderable;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public abstract class StaticObject extends GameObject implements Renderable {

    private static final Color DEFAULT_COLOR = Color.white;

    private Image image;
    private Color color;

    /**
     * Call this to initialize the image of this GameObject
     */
    protected abstract void initImage();


    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        if (image != null) {
            image.draw(getX(), getY());
        } else {
            Color color1 = g.getColor();
            g.setColor(color);
            g.fill(getBounds());
            g.setColor(color1);
        }
    }

    public Image getImage() {
        return image;
    }

    protected void setImage(Image image) {
        this.image = image;
    }

    protected void setColor(Color color) {
        this.color = color;
    }
}
