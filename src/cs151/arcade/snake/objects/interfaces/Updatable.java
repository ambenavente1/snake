package cs151.arcade.snake.objects.interfaces;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public interface Updatable {
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta);
}
