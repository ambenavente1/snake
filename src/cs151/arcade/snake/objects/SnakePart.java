package cs151.arcade.snake.objects;

import org.newdawn.slick.geom.Vector2f;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/14
 */
public abstract class SnakePart extends StaticObject {

    public static final int DEFAULT_SIZE = 16;

    /**
     * Creates a new SnakePart object
     *
     * @param pos the position to create it at
     */
    public SnakePart(Vector2f pos) {
        setPos(pos);
        setWidth(DEFAULT_SIZE);
        setHeight(DEFAULT_SIZE);
    }
}
