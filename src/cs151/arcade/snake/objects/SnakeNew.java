package cs151.arcade.snake.objects;

import cs151.arcade.snake.main.SnakeGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/14
 */
public class SnakeNew extends Snake {

    private float angle;

    public SnakeNew() {
        super(SnakeCircle.class);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        angle += checkPlayerInput(container.getInput());

        if (addElapsedTime(delta) > Snake.MOVE_TIME) {
            setTimeSinceMove(0);
            Vector2f newPos = getHeadPos();

            newPos.x += SnakePart.DEFAULT_SIZE * Math.cos(angle);
            newPos.y += SnakePart.DEFAULT_SIZE * Math.sin(angle);

            move(newPos);

            // Printed out the nodes for debugging purposes
//            System.out.println(this);
        }
    }

    /**
     * Checks for the player's input and depending on what was pressed,
     * returns the amount to rotate the snake
     *
     * @param input input manager of the GameContainer
     * @return the amount to rotate the snake
     */
    private float checkPlayerInput(Input input) {
        final float DEFAULT_MOVE = 0.2f;

        float result = 0.0f;

        if (input.isKeyDown(Input.KEY_LEFT)) {
            result = -DEFAULT_MOVE;
        }

        if (input.isKeyDown(Input.KEY_RIGHT)) {
            result = DEFAULT_MOVE;
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Object p : parts) {
            sb.append("{" + ((SnakePart)p).getPos() + "},\n");
        }

        return sb.toString();
    }

    @Override
    public boolean isDead() {
        for (int i = 2; i < parts.size(); i++) {
            if (parts.get(i).getBounds().intersects(getHeadBounds())) {
                return true;
            }
        }
        return getHeadPos().x < 0 ||
                getHeadPos().x > SnakeGame.GAME_WIDTH ||
                getHeadPos().y < 0 ||
                getHeadPos().y > SnakeGame.GAME_HEIGHT;
    }
}
