package cs151.arcade.snake.objects;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public class GameObject {

    private Vector2f pos;
    private int width;
    private int height;
    private Rectangle bounds;

    public GameObject() {
        this(new Vector2f(0, 0));
    }

    public GameObject(Vector2f pos) {
        this.pos = pos;
        this.width = 0;
        this.height = 0;
        this.bounds = new Rectangle(pos.x, pos.y, width, height);
    }

    public float getX() {
        return pos.x;
    }

    public void setX(float x) {
        setPos(new Vector2f(x, pos.y));
    }

    public float getY() {
        return pos.y;
    }

    public void setY(float y) {
        setPos(new Vector2f(pos.x, y));
    }

    public Vector2f getPos() {
        return pos;
    }

    public void setPos(Vector2f pos) {
        this.pos = pos;
        updateBounds();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        updateBounds();
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
        updateBounds();
    }

    public Rectangle getBounds() {
        return bounds;
    }

    private void updateBounds() {
        bounds.setBounds(pos.x, pos.y, width, height);
    }

}
