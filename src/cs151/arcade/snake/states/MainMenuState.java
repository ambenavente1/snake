package cs151.arcade.snake.states;

import cs151.arcade.snake.main.SnakeGame;
import cs151.arcade.snake.objects.Snake;
import cs151.arcade.snake.objects.SnakeTitle;
import cs151.arcade.snake.sounds.SoundManager;
import cs151.arcade.snake.ui.Control;
import cs151.arcade.snake.ui.ControlManager;
import cs151.arcade.snake.ui.Label;
import cs151.arcade.snake.ui.LinkLabel;
import cs151.arcade.snake.ui.events.ActionArgs;
import cs151.arcade.snake.ui.events.ActionDoer;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.awt.Font;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public class MainMenuState extends BasicGameState {

    private static final int X1 = 16 * 4;
    private static final int X2 = SnakeGame.GAME_WIDTH - 16 * 4;
    private static final int Y1 = 16 * 2;
    private static final int Y2 = SnakeGame.GAME_HEIGHT - 16 * 2;

    private ControlManager controls;
    private StateBasedGame parentGame;
    private Image s;
    private SnakeTitle snakeTitle;
    private Color backgroundColor;

    public MainMenuState(StateBasedGame parentGame) {
        this.parentGame = parentGame;
    }

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.MAIN_MENU.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        TrueTypeFont titleFont = new TrueTypeFont(new Font("Myriad-Pro " +
                "Regular", Font.PLAIN, 48), true);
        TrueTypeFont labelFont = new TrueTypeFont(new Font("Myriad-Pro " +
                "Regular", Font.PLAIN, 20), true);
        backgroundColor = new Color(0x99cc99);
        final int LABEL_PADDING = 15;
        s = new Image("res/s_vec.png");
        s.setFilter(Image.FILTER_LINEAR);

        controls = new ControlManager();

        Control title = new Label("lblTitle", "nake");
        title.setFont(titleFont);
        title.setWidth(titleFont.getWidth(title.getText()));
        title.setHeight(titleFont.getLineHeight());
        title.setPos(new Vector2f(SnakeGame.GAME_WIDTH / 2
                                        - title.getWidth() / 2 + 30, 90));
        controls.add(title);

        Vector2f startPos = title.getPos().copy();
        startPos.y += title.getHeight() + LABEL_PADDING*4;

        LinkLabel lblPlayClassic = new LinkLabel("lblPlayClassic",
                "Classic");
        lblPlayClassic.setFont(labelFont);
        lblPlayClassic.setWidth(labelFont.getWidth(lblPlayClassic.getText()));
        lblPlayClassic.setHeight(labelFont.getLineHeight());
        lblPlayClassic.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                SoundManager.MENU_BEEP.play();
                parentGame.enterState(States.GAMEPLAY_CLASSIC.getID(),
                                      new FadeOutTransition(),
                                      new FadeInTransition());
            }
        });
        startPos.x = SnakeGame.GAME_WIDTH / 2
                        - lblPlayClassic.getWidth() / 2;
        lblPlayClassic.setPos(startPos.copy());
        lblPlayClassic.setHighlightColor(Color.blue);
        controls.add(lblPlayClassic);

        startPos.y += lblPlayClassic.getHeight() + LABEL_PADDING;

        LinkLabel lblPlayNew = new LinkLabel("lblPlayNew", "Snake 2.0");
        lblPlayNew.setFont(labelFont);
        lblPlayNew.setWidth(labelFont.getWidth(lblPlayNew.getText()));
        lblPlayNew.setHeight(labelFont.getHeight());
        lblPlayNew.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                SoundManager.MENU_BEEP.play();
                parentGame.enterState(States.GAMEPLAY_NEW.getID(),
                                      new FadeOutTransition(),
                                      new FadeInTransition());
            }
        });
        startPos.x = SnakeGame.GAME_WIDTH / 2 - lblPlayNew.getWidth() / 2;
        lblPlayNew.setPos(startPos.copy());
        lblPlayNew.setHighlightColor(Color.blue);
        controls.add(lblPlayNew);

        startPos.y += lblPlayNew.getHeight() + LABEL_PADDING;

        LinkLabel lblQuit = new LinkLabel("lblQuit", "Quit");
        lblQuit.setFont(labelFont);
        lblQuit.setWidth(labelFont.getWidth(lblQuit.getText()));
        lblQuit.setHeight(labelFont.getHeight());
        lblQuit.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                SoundManager.MENU_BEEP.play();
                parentGame.enterState(States.QUIT.getID(),
                                      new FadeOutTransition(),
                                      null);
            }
        });
        startPos.x = SnakeGame.GAME_WIDTH / 2 - lblQuit.getWidth() / 2;
        lblQuit.setHighlightColor(Color.blue);
        lblQuit.setPos(startPos.copy());
        controls.add(lblQuit);
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
        super.enter(container, game);
        initSnake();
        container.getInput().clearKeyPressedRecord();
    }

    @Override
    public void leave(GameContainer container, StateBasedGame game) throws SlickException {
        super.leave(container, game);
        initSnake();
        container.getInput().clearKeyPressedRecord();
    }

    private void initSnake() {
        snakeTitle = new SnakeTitle(X1, Y1, X2, Y2);
        snakeTitle.setStarted(false);
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render
     * an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        g.setColor(Color.black);
        g.fillRect(0, 0, SnakeGame.GAME_WIDTH, SnakeGame.GAME_HEIGHT);
        g.setColor(Color.white);
        snakeTitle.render(container, game, g);
        controls.render(container, game, g);

        String names = "Anthony B + Daniel P";
        g.drawString(names, SnakeGame.GAME_WIDTH - g.getFont().getWidth
                        (names),
                SnakeGame.GAME_HEIGHT - g.getFont().getLineHeight()
        );
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        controls.update(container, game, delta);
        snakeTitle.update(container, game, delta);

        if (snakeTitle.getHeadPos().x < 0
         || snakeTitle.getHeadPos().x > SnakeGame.GAME_WIDTH
         || snakeTitle.getHeadPos().y < 0
         || snakeTitle.getHeadPos().y > SnakeGame.GAME_HEIGHT) {
            snakeTitle = new SnakeTitle(X1, Y1, X2, Y2);
        }
    }
}
