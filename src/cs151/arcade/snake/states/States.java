package cs151.arcade.snake.states;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/19/14
 */
public enum States {

    SPLASH(0),
    MAIN_MENU(1),
    GAMEPLAY(3),
    QUIT(4),
    GAMEPLAY_CLASSIC(5),
    GAMEPLAY_NEW(6);

    private final int ID;

    private States(int id) {
        ID = id;
    }

    public int getID() {
        return ID;
    }
}
