package cs151.arcade.snake.states;

import cs151.arcade.snake.main.SnakeGame;
import cs151.arcade.snake.objects.Food;
import cs151.arcade.snake.objects.SnakeNew;
import cs151.arcade.snake.objects.Snake;
import cs151.arcade.snake.sounds.SoundManager;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/14
 */
public class GameplayNewState extends BasicGameState {

    private static final int DEAD_TIME = 1000;

    private Snake snake;
    private Food food;
    private boolean gameOver;
    private int timeSinceDead;
    private int score;
    private Color backgroundColor;

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.GAMEPLAY_NEW.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        initGame();
    }

    private void initGame() {
        snake = new SnakeNew();
        food = new Food();
        gameOver = false;
        timeSinceDead = 0;
        score = 0;
        backgroundColor = new Color(0x000000);
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render
     * an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        g.setColor(backgroundColor);
        g.fillRect(0, 0, SnakeGame.GAME_WIDTH, SnakeGame.GAME_HEIGHT);
        g.setColor(Color.white);

        if (!gameOver) {
            g.setAntiAlias(true);
            snake.render(container, game, g);
            food.render(container, game, g);
            g.setAntiAlias(false);
            g.drawString("Score: " + score,
                         SnakeGame.GAME_WIDTH - 
                             g.getFont().getWidth("Score: " + score) -
                             10,
                         SnakeGame.GAME_HEIGHT -
                             g.getFont().getLineHeight() -
                             10);
        } else {
            g.drawString("G A M E  O V E R", SnakeGame.GAME_WIDTH / 2 -
                    g.getFont().getWidth("G A M E  O V E R") / 2,
                    SnakeGame.GAME_HEIGHT / 2 - g.getFont().getLineHeight()
                            / 2);
            g.drawString("Score: " + score,
                         SnakeGame.GAME_WIDTH / 2 - 
                           g.getFont().getWidth("Score: " + score) / 2,
                         SnakeGame.GAME_HEIGHT / 2 - 
                           g.getFont().getLineHeight() +
                           g.getFont().getLineHeight() * 2.5f);
        }
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        if (!gameOver) {
            if (snake.isDead()) {
                SoundManager.SNAKE_DEATH.play();
                gameOver = true;
            }
            snake.update(container, game, delta);

            if (snake.getHeadBounds().intersects(food.getBounds())) {
                SoundManager.FOOD_BEEP.play();
                snake.add();
                food.setRandPos();
                score++;
            }
        } else {
            if ((timeSinceDead += delta) >= DEAD_TIME) {
                game.enterState(States.MAIN_MENU.getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        }
    }

    @Override
    public void leave(GameContainer container,
                      StateBasedGame game) throws SlickException {
        initGame();
    }
}
