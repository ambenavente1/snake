package cs151.arcade.snake.states;

import cs151.arcade.snake.main.SnakeGame;
import cs151.arcade.snake.objects.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/2014
 */
public class GameplayState extends BasicGameState {

    private GameplayType type;
    private Snake snake;
    private Food food;
    private boolean gameOver;

    public GameplayState(GameplayType type) {
        this.type = type;
    }

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.GAMEPLAY.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        snake = type == GameplayType.CLASSIC ? new SnakeClassic()
                                             : new SnakeNew();
        food = new Food();
        gameOver = false;
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render
     * an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        snake.render(container, game, g);
        food.render(container, game, g);
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        snake.update(container, game, delta);

        boolean collision = false;

        if (type == GameplayType.CLASSIC) {
            collision = snake.getHeadPos().equals(food.getPos());
        } else if (type == GameplayType.NEW) {
            collision = snake.getHeadBounds().intersects(food.getBounds());
        }

        if (collision) {
            snake.add();
            food.setRandPos();
        }

        if (checkBounds()) {
            // TODO: Game over
        }

    }

    /**
     * Checks if the snake has left the game bounds
     *
     * @return if the snake has gone out of bounds
     */
    private boolean checkBounds() {
        return snake.getHeadPos().x < 0 ||
               snake.getHeadPos().x
                       > SnakeGame.GAME_WIDTH - SnakePart.DEFAULT_SIZE ||
               snake.getHeadPos().y < 0 ||
               snake.getHeadPos().y
                       > SnakeGame.GAME_HEIGHT - SnakePart.DEFAULT_SIZE;
    }
}
