package cs151.arcade.snake.sounds;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/2014
 */
public class SoundManager {
    public static Sound MENU_BEEP;
    public static Sound FOOD_BEEP;
    public static Sound SNAKE_DEATH;
    static {
        try {
            MENU_BEEP = new Sound("res/beep_1.wav");
            FOOD_BEEP = new Sound("res/beep_2.wav");
            SNAKE_DEATH = new Sound("res/bit_death.wav");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
